<a class="btn btn-warning edit-btn float-right" href="{{route('post.edit', $post->id)}}">Edit</a>

<form action="/posts/{{$post->id}}" method="POST">
    <input type="hidden" name="_method" value="DELETE">
    @csrf   
    <input type="submit" class="btn btn-danger delete-btn float-right" value="Delete">
</form>
            