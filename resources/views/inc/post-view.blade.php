<div class="post-box">
     <div class="row">
        <div class="col-4 text-center">
          <img src="{{asset('1.jpg')}}" alt="..." class="post-img">
          <br>
          <span>Likes({{ count($post->likes) }}) | Dislikes({{ count($post->dislikes) }})</span>
        </div>
        <div class="text col-6">
          <h4 class="h4 font-weight-bold"> <a class="secondary-color title-link" href="{{route('show-post', $post->id)}}">{{$post->title}}</a></h4>
          <p class="font-weight-light">{{$post->description}}</p>
          <hr>
          <p class="font-weight-light font-italic">{{strlen($post->body) > 160 ? substr($post->body, 0,  160) : $post->body}}
            <span><a href="{{route('show-post', $post->id)}}"> ... full post</a></span>
          </p>
        </div>
        <div class="col text-center">
          <img src="{{asset('avatar.jpg')}}" alt="" class="avatar">
          <h6>by <span class="secondary-color"><a href="{{route('profile', $post->user->id)}}" class="secondary-color title-link">{{$post->user->username}}</a></span></h6>
          <p>{{ $post->created_at->format('M / d / Y') }} </p>
        </div>
     </div>   
</div>
