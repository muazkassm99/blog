@extends('layouts.app')

@section('title', '| Profile')

@section('content')


<div class="jumbotron">

    {{-- likes and dislikes --}}
    <div id="profile-stats" class="text-center">
        
        <h4>Top Post :  
            @if ($toppost)
            <a href="{{route('show-post', $toppost->id)}}">this</a>
            
            @else
                --
            @endif

        </h4>
        <h4>Likes ({{$likes}})</h4>
        <h4>Dislikes ({{$dislikes}})</h4>
    </div>

    {{-- Create and Edit --}}
    <div id="profile-manipulation" class="text-center">

        @if (Auth::check())
            
            @if(Auth::id() == $user->id)
            <a class="btn btn-warning block" id="edit-profile-btn" href="{{Route('profile.edit', $user->id)}}">Edit Profile</a>
            <br>
            <a class="btn btn-success block" href="{{route('post.create')}}">Create Post</a>
            @endif
        @endif
    </div>

    <div class="text-center">
        <div id="profile-img">
            <img class="avatar-profile" src="{{asset('avatar.jpg')}}" alt="">

        </div>
        <div id="profile-details">
            <h4 class="secondary-color">{{ $user->username }}</h4>
            <div id="profile-bio" class="secondary-color">
                @if (!empty($user->bio))

                {{ $user->bio }}

                @else
                {{"No Bio Available"}}
                @endif


            </div>
            <br>
            <h5>{{ $user->email }}</h5>

            <p>Joined at : {{$user->created_at->format('M / d / Y') }}</p>

        </div>
    </div>



</div>


@if (count($posts)>0)
{{-- Display the posts --}}
@foreach ($posts as $post)

<div class="post-box">
    <div class="row">
        <div class="col-4 text-center">
            <img src="{{asset('1.jpg')}}" alt="..." class="post-img">
            <p>{{ $post->created_at->format('M / d / Y') }} </p>
            <hr>
            <span>Likes({{ count($post->likes) }}) | Dislikes({{ count($post->dislikes) }})</span>

        </div>
        <div class="text col">

            @if (Auth::check())
            @if(Auth::id() == $user->id)
            @include('inc.edit-delete')
            @endif
            @endif
            <h4 class="h4 font-weight-bold"> <a class="secondary-color title-link"
                    href="{{route('show-post', $post->id)}}">{{$post->title}}</a></h4>
            <p class="font-weight-light">{{$post->description}}</p>
            <hr>
            <p class="font-weight-light font-italic">
                {{strlen($post->body) > 160 ? substr($post->body, 0,  160) : $post->body}}
                <span><a href="{{route('show-post', $post->id)}}"> ... full post</a></span>
            </p>
        </div>
    </div>
</div>
<br>
<hr><br>


@endforeach


@else
{{-- No posts to display --}}
<h4>No Posts To Display</h4>
@endif



@endsection
