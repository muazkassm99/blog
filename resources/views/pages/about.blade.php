@extends('layouts.app')

@section('title', '| About')

@section('content')
    <div class="jumbotron">

        <h3>About Me</h3>
        <p>
            My name is Mouaz Al-Kassm and I am a sophomore college student.
            <br>
            I am very passionate about web development and this is my first website
            made with laravel.
        </p>

    </div>
@endsection