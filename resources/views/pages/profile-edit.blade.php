@extends('layouts.app')

@section('title', '| Edit Profile')

@section('content')
<br>
<div class="form-group">
    <form action="/users/{{$user->id}}" method="POST">
        @method('PUT')
        @csrf
        <label for="fullname">FullName:</label>
        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="User fullname ..."
            value="{{old('fullname', $user->fullname)}}">
        <br>
        <label for="bio">Biography:</label>
        <input type="text" class="form-control" id="bio" name="bio" placeholder="User biography ..."
            value="{{old('fullname', $user->bio)}}">
        <br>
<button class="btn btn-primary" type="submit">EDIT</button>
</form>
</div>
@endsection
