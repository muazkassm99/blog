@extends('layouts.app')

@section('title', '| Home')

@section('content')

    <div class="jumbotron text-center">
        <h1> <strong> Welcome to <span class="secondary-color">Kangaroo</span>  </strong> </h1>
        <h3>The place where you can share your thoughts freely</h3>
        <p>Post - Share - Express Yourself - Interact - Meet new people</p>
        
    </div>

    @if (count($posts)>0)
    {{-- Display the posts --}}
        <br>
        @foreach ($posts as $post)

            @include('inc.post-view')
            {{-- <a href="/posts/{{$post->id}}/show" class="btn btn-info">View Post</a> --}}
            <br><hr><br>
        @endforeach


    @else
    {{-- No posts to display --}}
    <h4>No Posts To Display</h4>
    @endif
@endsection