@extends('layouts.app')

@section('title', '| Archive')




@section('content')

<div class="row justify-content-start">
    <div class="col-2"></div>
    <div class="search-form text-center col-8">
        <form action="/archive/search" method="GET">
            @csrf
            <div class="input-group">
                <input class="form-control" type="search" name="search">
                <span class="input-group-prepend">
                    <button type="submit" class="btn btn-secondary">Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<hr>

@if (count($posts)>0)
{{-- Display the posts --}}
<br>
@foreach ($posts as $post)

{{-- <ul class="list-group list-group-flush">
                <li class="list-group-item">{{$post->title}} by : {{$post->user->username}}</li>
<li class="list-group-item">{{$post->description}}</li>
<li class="list-group-item">{{strlen($post->body) > 160 ? substr($post->body, 0,  160).' ...' : $post->body}}</li>
</ul>
<a href="/posts/{{$post->id}}/show" class="btn btn-info">View Post</a> --}}
@include('inc.post-view')
<br>
<hr><br>

@endforeach

{{$posts->links()}}

@else
{{-- No posts to display --}}
<h4>No Posts To Display</h4>
@endif
@endsection
