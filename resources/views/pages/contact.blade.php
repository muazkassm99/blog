@extends('layouts.app')

@section('title', '| Contact')

@section('content')
<div class="jumbotron">

    <h3>Conatct Me</h3>
    <p>
        You can contact me on Facebook, Instagram or Whatsapp ..
        <br><br>

        <div class="info">
            <i class="fa fa-facebook-square" aria-hidden="true"></i> : <a target="_blank" href="https://www.facebook.com/mouazalkassm">here</a>
            <br>
            <i class="fa fa-instagram" aria-hidden="true"></i> : <a target="_blank" href="https://www.instagram.com/muazkassm99/">here</a>
            <br>
            <i class="fa fa-whatsapp" aria-hidden="true"></i> : +963956134890
        </div>
    </p>

</div>
@endsection
    