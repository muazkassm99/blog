@extends('layouts.app')

@section('title', '| Post')

@section('content')

    <br><br>
    {{-- Image --}}
    <div class="row">
        <div class="col-12">
        <h1 class="font-weight-bold secondary-color">{{$post->title}}</h1>
        <h3 class="font-weight-light  font-italic">{{$post->description}}</h3>
        <hr>
        </div>
        <div class="col-9">
            <img src="{{asset('1.jpg')}}" alt="..." class="post-img">
            
        </div>

        <div class="col text-center">
            <img src="{{asset('avatar.jpg')}}" alt="" class="avatar">
            <div>
                <h5>By <span class="secondary-color"><a href="{{route('profile', $post->user->id)}}" class="secondary-color title-link">{{$post->user->username}}</a></span></h5>
            <p>{{ $post->created_at->format('M / d / Y') }} </p>
            </div>
            <a href="{{route('like', $post->id)}}" class="btn btn-info block">Like ({{$likes}})</a>
            <br>
            <a href="{{route('dislike', $post->id)}}" class="btn btn-danger block">Dislike ({{$dislikes}})</a>
            <br>
            @if (Auth::check())
                @if(Auth::user()->username == $post->user->username)
                    @include('inc.edit-delete')       
                @endif
            @endif
            
        </div>


        <div class="col-12 box">
            <p class="h4">{{$post->body}}
        </div>
    </div>
    


@endsection

