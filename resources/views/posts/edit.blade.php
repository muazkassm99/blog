@extends('layouts.app')

@section('title', '| Post')

@section('content')
<div class="form-group">
    <form action="/posts/{{$post->id}}" method="POST">
        @csrf
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Post Title ..." value="{{old('title', $post->title)}}">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Describle your post .." value="{{old('description', $post->description)}}">
        </div>
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label for="body">Example textarea</label>
            <textarea class="form-control" id="body" name="body" rows="3">{{old('body', $post->body)}}</textarea>
        </div>
        <button class="btn btn-primary" type="submit">EDIT</button>
    </form>
</div>
@endsection



