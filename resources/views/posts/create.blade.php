@extends('layouts.app')

@section('title', '| Post')

@section('content')
<div class="form-group">
    <form action="/posts" method="POST">
        @csrf
        
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Post Title ...">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Describle your post ..">
        </div>
        
        <div class="form-group">
            <label for="body">Example textarea</label>
            <textarea class="form-control" id="body" name="body" rows="3"></textarea>
        </div>
        <button class="btn btn-primary" type="submit">POST</button>
        
    </form>
</div>
@endsection



