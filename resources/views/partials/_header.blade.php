<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container in-nav">
          <a class="navbar-brand" href="{{route('index')}}"> <span class="secondary-color"> Kangaroo </span></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
  
          <div class="collapse navbar-collapse" id="navbarsExample07">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item {{Route::is('index') ?  'active' : ''}}">
                <a class="nav-link" href="{{route('index')}}">Home</a>
              </li>
              <li class="nav-item {{Route::is('archive') ?  'active' : ''}}">
                <a class="nav-link" href="{{route('archive')}}">Archive</a>
              </li>

              @if (Auth::check())

                <div class="dropdown show dropdown-menu-right floatright">
                  <a class="nav-item nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->username}}
                  </a>
                
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="{{route('profile', Auth::id())}}">My Profile</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                      </a>
                    <a class="dropdown-item" href="{{route('contact')}}">Contact Us</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </div>
                </div>
              @else
                  <div class="dropdown show dropdown-menu-right ">
                    <a class="nav-item nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Connect
                    </a>
                  
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="{{route('register')}}">Join</a>
                      <a class="dropdown-item" href="{{route('login')}}">Login</a>
                      <a class="dropdown-item" href="{{route('about')}}">About Us</a>
                      <a class="dropdown-item" href="{{route('contact')}}">Contact Us</a>
                    </div>
                  </div>
              @endif
              

            </ul>
            
          </div>
        </div>
      </nav> 
      
      {{-- <nav>
        <div class="logo">
          <a href="">Kangaroo</a>
        </div>
        <ul class="nav-links navbar-nav mr-auto">
          <li class="nav-item">
            <a href="">Home</a>
          </li>
          <li>
            <a href="">Archive</a>
          </li>
          <li>
            <a href="">About</a>
          </li>
        </ul>
      </nav>
    </nav> --}}
  </header>