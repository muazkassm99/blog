<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\User;
use Auth;   

class PagesController extends Controller
{

    //returns the home page
    public function getIndex(){
        $posts = Post::orderBy('created_at', 'desc')
        ->take(5)
        ->get();

        return view('pages.index')->with('posts', $posts);
    }


    //returns archive page with 5 posts in a page.
    public function getArchive(){
        $posts = Post::orderBy('created_at', 'desc')
        ->paginate(5);
        return view('pages.archive')->with('posts', $posts);
    }

    //searches for something relative to the word and returns an Archive page

    public function search(Request $request){
        $words = $request->input('search');

        //$posts = DB::table('posts')->where('title', 'like', '%'.$words.'%')->get();
        $posts = Post::query()
            ->where('title', 'LIKE', "%{$words}%")
            ->paginate(5); 
        //return $posts;
        //return $posts[0]->linkes;
        return view('pages.archive')->with('posts', $posts);
    }


    //returns the profile requested.
    public function getProfile($id){

        // getting all posts sorted
        $posts = Post::where('user_id', $id)
        ->orderBy('created_at', 'desc')
        ->paginate(5);

        // getting the user
        $user = User::find($id);

        //getting the number of likes and dislikes
        $likes = $this->getLikes($posts);
        $dislikes = $this->getDislikes($posts);

        // getting the top post
        $toppost = $this->getTopPost($posts);

        return view('pages.profile')
            ->with('user', $user)
            ->with('posts', $posts)
            ->with('likes', $likes)
            ->with('dislikes', $dislikes)
            ->with('toppost', $toppost);
    }

    //returns the about us page.
    public function getAbout(){
        return view('pages.about');
    }

    //returns the contact me page.
    public function getContact(){
        return view('pages.contact');
    }

    //returns the profile edit page
    public function getProfileEdit($id){

        $user = User::find($id);
        if(!$user){
            return redirect()->back();
        }
        return view('pages.profile-edit')->with('user', $user);
    }



    // PRIVATE METHODS FOR HELPING IN MAKING RESPONSES
    private function getLikes($posts)
    {
        $likes = 0;
        foreach ($posts as $post) {
            $likes += count($post->likes);
        }
        return $likes;
    }
    private function getDislikes($posts)
    {
        $dislikes = 0;
        foreach ($posts as $post) {
            $dislikes += count($post->dislikes);
        }
        return $dislikes;
    }
    private function getTopPost($posts){
        $top =0;
        $chosen = null;
        foreach ($posts as $post) {
            if(count($post->likes) > $top){
                $top = count($post->likes);
                $chosen = $post;
            }
        }
        return $chosen;
    }



}
