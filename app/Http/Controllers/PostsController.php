<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Post;
use Auth;
use App\Like;
use App\Dislike;


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:120',
            'description' => 'required|max:255', 
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $post = new Post;
        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->body = $request->input('body');
        $post->user_id = Auth::id();
        $post->save();

        return redirect('/profile/'.Auth::id())->with('success', 'Post was Uploaded successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $likes = Like::where('post_id', $id)->count();
        $dislikes = Dislike::where('post_id', $id)->count();
        if($post){
            return view('posts.show')
                ->with('post', $post)
                ->with('likes', $likes)
                ->with('dislikes', $dislikes);
        }else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if(!$post){
            return redirect()->back();
        }
        $edit = true;
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:120',
            'description' => 'required|max:255', 
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $post = Post::find($id);

        if($post){
            $post->title = $request->input('title');
            $post->description = $request->input('description');
            $post->body = $request->input('body');
            $post->save();
        }else{
            return redirect()->back();
        }

        return redirect('/profile/'.Auth::id())->with('success', 'Post was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/profile/'.Auth::id())->with('success', 'Post was deleted successfuly');
    }

    public function like($id){

        if($this->checkForLikeRelation(Auth::id(), $id)){ // user didn't like this before
            
            //remove dislikes..
            Dislike::where(['user_id'=>Auth::id(), 'post_id'=>$id])->delete();
            
            $like = new Like;
            $like->user_id = Auth::id();
            $like->post_id = $id;
            $like->save();
            return redirect()->back()->with('status', 'you already like this post!');
        }else{
            return redirect()->back();
        }
    }
    
    public function dislike($id){
        if($this->checkForDislikeRelation(Auth::id(), $id)){ // user didn't like this before
            
            //remove like..
            Like::where(['user_id'=>Auth::id(), 'post_id'=>$id])->delete();
            
            $dislike = new Dislike;
            $dislike->user_id = Auth::id();
            $dislike->post_id = $id;
            $dislike->save();
            return redirect()->back()->with('status', 'you already dislike this post!');
        }else{
            return redirect()->back();
        }
    }

    /*
        This functions returns true if the user passed in has liked
        the post passed it
        and false if he has not!
    */
    private function checkForLikeRelation(int $user_id, int $post_id){

        $relation = Like::where(['user_id'=>$user_id, 'post_id'=>$post_id])->first();

        if(empty($relation->user_id)){
            return true;
        }else{
            return false;
        }
    }
    /*
        This functions returns true if the user passed in has disliked
        the post passed it
        and false if he has not!
    */
    private function checkForDislikeRelation(int $user_id, int $post_id){

        $relation = Dislike::where(['user_id'=>$user_id, 'post_id'=>$post_id])->first();

        if(empty($relation->user_id)){
            return true;
        }else{
            return false;
        }
    }
    public function search(Request $request){
        $q = $request->input('q');
        // $user = User::where('name','LIKE','%'.$q.'%')->orWhere('email','LIKE','%'.$q.'%')->get();
        // if(count($user) > 0)
        // return view('welcome')->withDetails($user)->withQuery ( $q );
        // else return view ('welcome')->withMessage('No Details found. Try to search again !');


        return $q;
    }
}
