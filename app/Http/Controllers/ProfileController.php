<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;   
use App\User;
use Auth;

class ProfileController extends Controller
{
    public function updateProfile(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:20',
            'bio' => 'max:120'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::find($id);

        if($user){
            $user->fullname = $request->input('fullname');
            $user->bio = $request->input('bio');
            $user->save();
        }else{
            return redirect()->back();
        }

        return redirect('/profile/'.Auth::id())->with('success', 'Profile was edited successfully');
    }
}
