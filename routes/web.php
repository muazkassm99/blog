<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web', 'auth']], function () {
    
    

    //posts
    Route::resource('posts', 'PostsController');
    Route::get('/posts/create', 'PostsController@create')->name('post.create');
    Route::get('/posts/{id}/edit', 'PostsController@edit')->name('post.edit');
    

    //interaction with posts
    Route::get('/posts/like/{id}', 'PostsController@like')->name('like');
    Route::get('/posts/dislike/{id}', 'PostsController@dislike')->name('dislike');


    //profile manipulation
    Route::get('/profile/{id}/edit/', 'PagesController@getProfileEdit')->name('profile.edit');
    Route::put('/users/{id}', 'ProfileController@updateProfile');
});



Route::get('/', 'PagesController@getIndex')->name('index');
Route::get('/archive', 'PagesController@getArchive')->name('archive');


Auth::routes();

// Route::redirect('/home', '/');

Route::get('/about', 'PagesController@getAbout')->name('about');

Route::get('/contact', 'PagesController@getContact')->name('contact');

Route::view('home', 'home');

Route::get('/posts/{id}/show', 'PostsController@show')->name('show-post');


//profile
Route::get('/profile/{id}', 'PagesController@getProfile')->name('profile');

Route::get('/archive/search', 'PagesController@search');